import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import gameData from './database';

const Row = styled.div`
    width: 100%;
    height: 100%;
`;

const Column = styled.div`
  display: flex;
  flex-flow: column wrap;
`;

const Container = styled.div`
  width: 1000px;
  height: 1000px;
  margin:5rem auto;
  position: relative;
`;

const Child = styled.div`
  width: 200px;
  height: 200px;
  float: left;
  border: 1px solid #ddd;
  padding: 2rem;
`;

const Button = styled.button`
  border: none;
  background: orange;
  padding: 1rem 2rem;
  margin: 2rem 0;
  color: #fff;
  font-weight: bold;
  border-radius: 2px;
`

const App = () => {

  const [data, setData] = useState(gameData);
  const [comparator, setComparator] = useState([]);
  const [timer, setTimer] = useState(30);
  let timeInter;

  useEffect(() => {
    if(comparator.length === 2)
      setTimeout(()=> {
        compare(comparator)
      }, 1000)
    
  }, [comparator.length]);

  useEffect(()=> {
    if(timer > 0 && timer < 30) {
      setTimeout(() => {
        setTimer(timer - 1);
      }, 1000);  
    }
  }, [timer]);

  function flipper() {
    const newData = gameData.map((el)=> ({...el, isVisible: false}) );
    debugger
    setData(newData)
  }

  const getDataIndex = (elm) => data.findIndex((el) => el.index === elm.index);

  function compare(elms) {
    const [first, second] = elms;
    const firstIndex = getDataIndex(first);
    const secondIndex = getDataIndex(second);

    if(first.img === second.img) {
      first.img = '/images/valid.svg';
      second.img = '/images/valid.svg';

      data[firstIndex] = first;
      data[secondIndex] = second;

      const newData = Array.from(data);
      setData(newData);
      setComparator([]);
    }else {

      first.isVisible = false;
      second.isVisible = false;

      data[firstIndex] = first;
      data[secondIndex] = second;

      const newData = Array.from(data);
      setData(newData);
      setComparator([]);
    }
  }

  const flipCard = (el) => {
    if(!el.isVisible && timer !== 0) {
      const elIndex = getDataIndex(el)
    
      const current =  data[elIndex];
      current.isVisible = !current.isVisible;
      data[elIndex] = current;
      const newData = Array.from(data)
      setData(newData);
      const elementToCompare = comparator;
  
      elementToCompare.push(current);
      setComparator(elementToCompare);
    }

  }
  function shuffle() {
    for (let i = data.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [data[i], data[j]] = [data[j], data[i]];
    }
    const newData = Array.from(data);
    setData(newData);
}
function countDown() {
  setTimer(timer - 1);
}
const FinishGame = styled.span`
  color: red;
`;

const Wrapper = styled.div`
  height: 15rem;
`
 const GameOver = () => {
   return (
    <FinishGame>
       Game Over!!!
    </FinishGame>
   )
 }
  return (
    <Container>
      <Wrapper>
        <Button onClick={flipper}>Flip</Button>
        <Button onClick={() => shuffle()}>Shuffle</Button>
        <Button onClick={countDown}>Start</Button>
        <Button onClick={()=> {
          window.location.reload(true);
        }}>Replay</Button>
        <h1>TIme: {timer} {timer === 0 && <GameOver />}</h1>
      </Wrapper>
      {/* <GameOver /> */}
      <Row>
        {data.map( el => 
          <Child onClick={() => {
          flipCard(el);
        }} key={el.index}>
            {!el.isVisible?
              <h2>{el.index}</h2>:
              <img src={el.img}  width="150px" height="150px"/>
            }
        </Child>
        )}
      </Row>
    </Container>
  );
}

export default App;
