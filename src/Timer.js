import React, { useEffect, useState } from 'react';
import { Button } from './App';

const Timer = () => {
    const [timer, setTimer] = useState(40);
    let timeInter;

    useEffect(()=> {
        if(timer > 0 && timer < 40) {
          setTimeout(() => {
            setTimer(timer - 1);
          }, 1000);  
        }
    }, [timer]);
      
    function countDown() {
        setTimer(timer - 1);
    }
    // function startTimer() {
    //     timeInter = setInterval(countDown, 1000)  
    // }

    return (
        <>
             <Button onClick={countDown}>Start</Button>
            <h1>TIme: {timer}</h1>
        </>
    )
};

export default Timer;